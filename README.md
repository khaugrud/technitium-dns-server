# Unofficial Docker Container for Technitium DNS/DHCP Server
A Local DNS and DHCP Server with AD Blocking

**Current Version - 13.0.4**

Gitlab Repo [https://gitlab.com/khaugrud/technitium-dns-server](https://gitlab.com/khaugrud/technitium-dns-server)

Visit [Technitium](https://technitium.com/dns/) for more information.

![Overview](https://technitium.com/dns/ScreenShot1.png)

Technitium DNS Server is an open source tool that can be used for self hosting a local DNS server for privacy & security or, used for experimentation/testing by software developers on their computer. It works out-of-the-box with no or minimal configuration and provides a user friendly web console accessible using any web browser.

## Block Ads At DNS Level
Technitium DNS Server allows you to configure Block List URLs that gets automatically updated daily to block ads on your network. The Quick Add option lists popular block lists available for you to choose from.

![DNS Blocking](https://technitium.com/dns/ScreenShot3.png)

### Privacy & Security
Technitium DNS Server supports using DNS-over-TLS and DNS-over-HTTPS protocols for forwarders allowing you to use popular public DNS resolvers like Cloudflare, Google & Quad9. These protocols provides privacy by encrypting your DNS traffic on the network and protects you from man-in-the-middle attacks.

![DNS-Over-HTTPS](https://technitium.com/dns/ScreenShot2.png)

### Features

- Works on Windows, Linux, macOS and Raspberry Pi.
- Installs in just a minute and works out-of-the-box with zero configuration.
- Block Ads using one or more block list URLs.
- Run DNS-over-TLS and DNS-over-HTTPS DNS service on your network.
- Use public DNS resolvers like Cloudflare, Google & Quad9 with DNS-over-TLS and DNS-over-HTTPS protocols as forwarders.
- Advance caching with features like serve stale, prefetching and auto prefetching.
- Supports working as an authoritative as well as a recursive DNS server.
- CNAME cloaking feature to block domain names that resolve to CNAME which are blocked.
- QNAME minimization support in recursive resolver.
- QNAME randomization support for UDP transport protocol.
- ANAME propriety record support to allow using CNAME like feature at zone root.
- APP propriety record support that allows custom DNS Apps to directly handle DNS requests and return a custom DNS response based on any business logic.
- Support for features like Split Horizon and Geolocation based responses using DNS Apps feature.
- Primary, Secondary, Stub, and Conditional Forwarder zone support.
- Host domain names on your own DNS server.
- Wildcard sub domain support.
- Enable/disable zones and records to allow testing with ease.
- Built-in DNS Client with option to import responses to local zone.
- Supports out-of-order DNS request processing for DNS-over-TCP and DNS-over-TLS protocols.
- Built-in DHCP Server that can work for multiple networks.
- IPv6 support in DNS server core.
- HTTP & SOCKS5 proxy support which can be configured to route DNS over Tor Network or use Cloudflare's hidden DNS resolver.
- Web console portal for easy configuration using any web browser.
- Built-in system logging and query logging.
- Open source cross-platform .NET 5 implementation hosted on [GitHub](https://github.com/TechnitiumSoftware/DnsServer).

### Docker

`docker run \
-d \
--name technitium-dns \
--hostname [Your Hostname] \
--restart always \
-p 5380:5380/tcp \
-p 53:53/tcp \
-p 53:53/udp \
-p 67:67/udp \
-v [path]:/app/config \
khaugrud/technitium-dns-server:latest`
